# Giphy Android Browser
A small standalone Giphy browser application: Gifs

<img src="/app/src/main/res/mipmap-xxxhdpi/ic_launcher_round.png"/>

## Screenshots
<p align="top">
<img src="/screenshots/screen_trending.jpg" width="20%"/>
<img align="top" src="/screenshots/screen_search_gif.jpg" width="20%"/>
<img align="top" src="/screenshots/screen_gif_detail_2.jpg" width="20%"/>
<img align="top" src="/screenshots/gifs-app-demo.gif" width="20%"/>
</p>

## Functionality
1. The entry point for the app is an "infinitely scrolling" list of the trending GIFs on Giphy.
2. There's a search bar on top that will allow a user to search for GIFs. These results will
replace the trending GIFs.
3. When you click a GIF, it will take you to a fullscreen view of that GIF.

## Architecture
### RIB
There are many ways to architect android app via MVP, VIPER, MVVM, MVI. In this sample app, I picked the [RIB architecture](https://github.com/uber/RIBs/wiki) that I worked in the past. RIB (_Router-Interactor-Builder_) is a cross-platform architecture framework. It does not tie to any specific architecture paradim i.e we can apply either MVP/MVI/MVVM to RIBs.
<br><br>
Each RIB defines its own functionality e.g. search, browse so it forces us to think through the architecture before coding. More importantly RIB allows us to separate the business logic from its UI. All the business logic is handled in the _Interactor_ See `SearchingGifInteractor`, therefore, Views are "dumb" e.g. `SearchBarView`. RIBs are re-usable too. For instance, `GifDetail` RIB is attached to both `SearchingGif` and `TrendingGif` RIBs for displaying a fullscreen Gif.
<br><br>
Here is the _5-RIB_ tree diagram used in the `Gifs` app
<img src="/screenshots/gifs_app_rib_tree.png" width="100%"/>

### Scoping
In addition to the app scope i.e. see `AppModule`, `NetworkModule`, each RIB has its own scope. Therefore we do not waste unnecessary resources. Each child RIB inherits its ancestors' scopes via `Builder.ParentComponents`. This is done in the RIB's builder class e.g. `TrendingGifBuilder`, `GifDetailBuilder` etc.
<br><br>
For example, `SearchQueryStream` is provided at the root scope, as a result, both its child RIBs `SearchingGif` and `SearchBar` can access to it via its DI graph.

### Single Activity
See `RootActivity`. No fragments.

## Giphy Endpoints
No need to bundle [Giphy SDK](https://developers.giphy.com/docs/sdk). We hit the following endpoints. The rule of thumb is always be very careful to integrate any 3rd party SDKs because of the apk size, security, and license etc.
- [Trending endpoint](https://developers.giphy.com/docs/api/endpoint#trending): `api.giphy.com/v1/gifs/trending`
- [Search endpoint](https://developers.giphy.com/docs/api/endpoint#search): `api.giphy.com/v1/gifs/search`

## JSON Structure
The data model is parsed via [Moshi](https://github.com/square/moshi/) (alternatively we can use [Gson](https://github.com/google/gson)). The model is used for both search and trending.

```
"data": [
    {
      "type": "gif",
      "id": "XeLTjxiVjyOgxG8QmF",
      "title": "Cant Do It Kristen Wiig GIF by The Academy Awards",
      ...
      "images": {
        ...
        "downsized_large": {
          "height": "259",
          "size": "1959840",
          "url": "https://media1.giphy.com/media/XeLTjxiVjyOgxG8QmF/giphy.gif",
          "width": "480"
        },
        "preview_gif": {
          "height": "54",
          "size": "48672",
          "url": "https://media1.giphy.com/media/XeLTjxiVjyOgxG8QmF/giphy-preview.gif",
          "width": "100"
        }
        ...
      },
      ...
    },
    ...
  }
]
```


## Build Tools & Versions Used
- Android Studio 3.6.3
- Minimum SDK level 21
- Java 1.8
- Gradle 3.6.3
- See `build.gradle` in the _project_ and _app_ module for more info

## Tech Stack & 3rd party libs
- 100% Java
- Google material design
- androidx ConstraintLayout / RecyclerView
- [RIB](https://github.com/uber/RIBs) framework
- Dagger 2
- RxJava/RxAndroid v2
- [AutoDispose](https://uber.github.io/AutoDispose/) - Rx lifecycle binding
- [RxBinding](https://github.com/JakeWharton/RxBinding)
- Retrofit2
- Http3
- [Moshi](https://github.com/square/moshi/) - Json parsing. 
- [Glide](https://bumptech.github.io/glide/) - Image cache & loading
- Timber - logging in debug mode
- Junit / Mockito
<br><br>
Note: I didn't explore too many `JetPack` components, however, alternatively we can also use `LiveData` to manage the state of the data in the `ViewModel`. I didn't use any persistent storage e.g. `Room` to store data on the device, as this is an overkill.  

## Focus Area
- App fondation
  - Build a solid app foundation for future iterations. There are many approaches to achieve the same result in terms of UIs so it's important to find the right one with reasonable tradeoffs.
- Readable production level code
  - Make sure the codebase is easier to read without adding additional comments.
- Clean UI
  - Minimal UI for MVP within the material design guideline. Pixel pushes later. 
- App efficiency 
  - Network/Data 
    - fetch smaller payload for gif data via the infinite paging
    - debounce search text for searching while typing
    - image caching via Glide
    - load preivew gifs for _trending_ list to save network bandwidth, and load original gifs for details 
  - UI
    - invalidate new inserted paging data only instead of the whole list
    - recycler view holder pattern
    - keep the view hierarchy flat
    - throttle for rx binding clicks
  - Import 3rd party libs only if needed
- Support the basic configuration change.
  - Device rotation
- Demonstrate unit tests for a (root) RIB. 
  - RIBs result in _smaller_ and testable classes. We only unit test _interactors_ and _routers_ (if needed).
  - See `RootInteractorTest`, `RootRouterTest`

## Test Devices
- Pixel 3 & Oneplus on Android 10
- Not tested on tablet devices
