package com.huzu.android.gify.browse.data.model;

import com.squareup.moshi.Json;

public final class MetaData {

  public int status;

  @Json(name = "msg")
  public String message;

  @Json(name = "response_id")
  public String responseId;
}
