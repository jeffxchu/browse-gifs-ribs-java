package com.huzu.android.gify.browse.features.search_bar;

import com.uber.rib.core.ViewRouter;

/**
 * Adds and removes children of {@link SearchBarBuilder.SearchBarScope}.
 */
public class SearchBarRouter extends
    ViewRouter<SearchBarView, SearchBarInteractor, SearchBarBuilder.Component> {

  SearchBarRouter(
      SearchBarView view,
      SearchBarInteractor interactor,
      SearchBarBuilder.Component component) {
    super(view, interactor, component);
  }
}
