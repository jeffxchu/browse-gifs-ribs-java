package com.huzu.android.gify.browse.data.model;

public final class GifData {

  public String id;

  public String type;

  public String username;

  public String title;

  public GifImageData images;
}
