package com.huzu.android.gify.browse.data.model;

import com.squareup.moshi.Json;

public final class GifImageData {

  @Json(name = "preview_gif")
  public GifImage previewGif;

  @Json(name = "downsized_large")
  public GifImage originalGif;
}
