package com.huzu.android.gify.browse.features.gif_detail;

import com.huzu.android.gify.browse.data.model.GifImage;
import com.uber.rib.core.Bundle;
import com.uber.rib.core.Interactor;
import com.uber.rib.core.RibInteractor;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import io.reactivex.Observable;

import static com.uber.autodispose.AutoDispose.autoDisposable;
import static io.reactivex.android.schedulers.AndroidSchedulers.mainThread;

/**
 * Coordinates Business Logic for {@link GifDetailBuilder.GifDetailScope}.
 *
 * The interactor that displays a gif image.
 */
@RibInteractor
public class GifDetailInteractor
    extends Interactor<GifDetailInteractor.GifDetailPresenter, GifDetailRouter> {

  @Inject
  GifDetailPresenter presenter;

  @Inject
  GifImage gifImage;

  @Inject
  Listener listener;

  @Override
  protected void didBecomeActive(@Nullable Bundle savedInstanceState) {
    super.didBecomeActive(savedInstanceState);
    presenter.renderGif(gifImage);

    presenter
        .dismissClicks()
        .observeOn(mainThread())
        .as(autoDisposable(this))
        .subscribe(__ -> listener.dismiss());
  }

  @Override
  protected void willResignActive() {
    super.willResignActive();
  }

  public interface Listener {

    void dismiss();
  }

  /**
   * Presenter interface implemented by this RIB's view.
   */
  interface GifDetailPresenter {

    void renderGif(GifImage image);

    Observable<Object> dismissClicks();
  }
}
