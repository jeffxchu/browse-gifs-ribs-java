package com.huzu.android.gify.browse.di.component;

import android.app.Application;

import com.huzu.android.gify.browse.App;
import com.huzu.android.gify.browse.di.module.ActivityBindingModule;
import com.huzu.android.gify.browse.di.module.AppModule;
import com.huzu.android.gify.browse.di.module.NetworkModule;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Component(
    modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        NetworkModule.class,
        ActivityBindingModule.class
    }
)
public interface AppComponent extends AndroidInjector<App> {

  @Component.Builder
  interface Builder {
    @BindsInstance
    Builder application(Application application);

    AppComponent build();
  }
}
