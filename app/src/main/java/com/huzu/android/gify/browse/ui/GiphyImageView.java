package com.huzu.android.gify.browse.ui;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

/**
 * Image view that respects its ratio based on its height and width.
 */
public class GiphyImageView extends AppCompatImageView {

  private float ratio;

  public GiphyImageView(Context context) {
    super(context);
  }

  public GiphyImageView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public GiphyImageView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public void setRatio(float ratio) {
    if (this.ratio != ratio) {
      this.ratio = ratio;
      requestLayout();
    }
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    if (ratio > 0.0f) {
      int width = MeasureSpec.getSize(widthMeasureSpec);
      int height = (int) (width * ratio);
      setMeasuredDimension(width, height);
    } else {
      super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
  }
}