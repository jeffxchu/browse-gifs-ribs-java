package com.huzu.android.gify.browse.root;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.huzu.android.gify.browse.R;

import androidx.annotation.Nullable;

/**
 * Top level view for {@link RootBuilder.RootScope}.
 */
public class RootView extends FrameLayout implements RootInteractor.RootPresenter {

  private ViewGroup topContainer;
  private ViewGroup contentContainer;

  public RootView(Context context) {
    this(context, null);
  }

  public RootView(Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public RootView(Context context, @Nullable AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    topContainer = findViewById(R.id.top_container);
    contentContainer = findViewById(R.id.content_container);
  }

  public ViewGroup getTopContainer() {
    return topContainer;
  }

  public ViewGroup getContentContainer() {
    return contentContainer;
  }
}