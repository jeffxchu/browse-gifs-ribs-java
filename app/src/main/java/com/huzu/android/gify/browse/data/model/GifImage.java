package com.huzu.android.gify.browse.data.model;

public final class GifImage {

  public String url;

  public int width;

  public int height;

  public int size;
}
