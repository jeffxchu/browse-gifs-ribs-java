package com.huzu.android.gify.browse.ui;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

/**
 * A helper {@link RecyclerView.ItemDecoration} class to keep the spacing evenly for the grid layout.
 */
public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

  private final int spanCount;
  private final int spacing;
  private final boolean includeEdge;

  public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
    super();
    this.spanCount = spanCount;
    this.spacing = spacing;
    this.includeEdge = includeEdge;
  }

  @Override
  public void getItemOffsets(
      Rect outRect,
      View view,
      RecyclerView parent,
      RecyclerView.State state) {
    super.getItemOffsets(outRect, view, parent, state);
    int position = parent.getChildAdapterPosition(view);
    int column = position % spanCount;

    if (includeEdge) {
      outRect.left = spacing - column * spacing / spanCount;
      outRect.right = (column + 1) * spacing / spanCount;

      if (position < spanCount) {
        outRect.top = spacing;
      }
      outRect.bottom = spacing;
    } else {
      outRect.left = column * spacing / spanCount;
      outRect.right = spacing - (column + 1) * spacing / spanCount;
      if (position >= spanCount) {
        outRect.top = spacing;
      }
    }
  }
}
