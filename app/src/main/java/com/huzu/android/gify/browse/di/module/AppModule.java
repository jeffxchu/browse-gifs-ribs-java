package com.huzu.android.gify.browse.di.module;

import android.app.Application;
import android.content.Context;

import com.huzu.android.gify.browse.data.GiphyApi;
import com.huzu.android.gify.browse.data.repository.GiphyRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

  @Provides
  Context provideContext(Application application) {
    return application.getApplicationContext();
  }

  @Provides
  GiphyRepository provideGiphyRepository(GiphyApi giphyApi) {
    return new GiphyRepository(giphyApi);
  }
}
