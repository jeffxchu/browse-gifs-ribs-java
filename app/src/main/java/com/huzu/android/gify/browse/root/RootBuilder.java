package com.huzu.android.gify.browse.root;

import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.huzu.android.gify.browse.App;
import com.huzu.android.gify.browse.R;
import com.huzu.android.gify.browse.di.component.AppComponent;
import com.huzu.android.gify.browse.di.module.AppModule;
import com.huzu.android.gify.browse.di.module.NetworkModule;
import com.huzu.android.gify.browse.features.search_bar.SearchBarBuilder;
import com.huzu.android.gify.browse.features.searching_gif.SearchingGifBuilder;
import com.huzu.android.gify.browse.features.trending_gif.TrendingGifBuilder;
import com.huzu.android.gify.browse.util.SearchQueryStream;
import com.uber.rib.core.ActivityContext;
import com.uber.rib.core.InteractorBaseComponent;
import com.uber.rib.core.ViewBuilder;

import java.lang.annotation.Retention;

import javax.inject.Qualifier;
import javax.inject.Scope;

import dagger.Binds;
import dagger.BindsInstance;
import dagger.Provides;

import static java.lang.annotation.RetentionPolicy.CLASS;

/**
 * Builder for the {@link RootScope}.
 */
public class RootBuilder
    extends ViewBuilder<RootView, RootRouter, RootBuilder.ParentComponent> {

  public RootBuilder(ParentComponent dependency) {
    super(dependency);
  }

  /**
   * Builds a new {@link RootRouter}.
   *
   * @param application     the {@link Application}.
   * @param parentViewGroup parent view group that this router's view will be added to.
   * @return a new {@link RootRouter}.
   */
  public RootRouter build(Application application, ViewGroup parentViewGroup) {
    RootView view = createView(parentViewGroup);
    RootInteractor interactor = new RootInteractor();
    Component component = DaggerRootBuilder_Component.builder()
        .parentComponent(getDependency())
        .view(view)
        .application(application)
        .context(parentViewGroup.getContext())
        .appComponent(((App) application).getAppComponent())
        .interactor(interactor)
        .build();
    return component.rootRouter();
  }

  @Override
  protected RootView inflateView(LayoutInflater inflater, ViewGroup parentViewGroup) {
    return (RootView) inflater.inflate(R.layout.rib_root, parentViewGroup, false);
  }

  public interface ParentComponent {
  }

  @dagger.Module
  public abstract static class Module {

    @RootScope
    @Binds
    abstract RootInteractor.RootPresenter presenter(RootView view);

    @RootScope
    @Provides
    static RootRouter router(
        Component component,
        RootView view,
        RootInteractor interactor) {
      return new RootRouter(
          view,
          interactor,
          component,
          new TrendingGifBuilder(component),
          new SearchingGifBuilder(component),
          new SearchBarBuilder(component));
    }

    @RootScope
    @Provides
    static SearchQueryStream provideSearchQueryStream() {
      return new SearchQueryStream();
    }
  }

  @RootScope
  @dagger.Component(
      modules = {Module.class, AppModule.class, NetworkModule.class},
      dependencies = {ParentComponent.class, AppComponent.class})
  interface Component extends
      InteractorBaseComponent<RootInteractor>,
      BuilderComponent,
      TrendingGifBuilder.ParentComponent,
      SearchBarBuilder.ParentComponent,
      SearchingGifBuilder.ParentComponent {

    @dagger.Component.Builder
    interface Builder {
      @BindsInstance
      Builder interactor(RootInteractor interactor);

      @BindsInstance
      Builder view(RootView view);

      @BindsInstance
      Builder application(Application application);

      @BindsInstance
      Builder context(@ActivityContext Context context);

      Builder appComponent(AppComponent appComponent);

      Builder parentComponent(ParentComponent component);

      Component build();
    }
  }

  interface BuilderComponent {
    RootRouter rootRouter();
  }

  @Scope
  @Retention(CLASS)
  @interface RootScope {
  }

  @Qualifier
  @Retention(CLASS)
  @interface RootInternal {
  }
}
