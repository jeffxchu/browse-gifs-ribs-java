package com.huzu.android.gify.browse.features.gif_detail;

import com.uber.rib.core.ViewRouter;

/**
 * Adds and removes children of {@link GifDetailBuilder.GifDetailScope}.
 */
public class GifDetailRouter extends
    ViewRouter<GifDetailView, GifDetailInteractor, GifDetailBuilder.Component> {

  GifDetailRouter(
      GifDetailView view,
      GifDetailInteractor interactor,
      GifDetailBuilder.Component component) {
    super(view, interactor, component);
  }
}
