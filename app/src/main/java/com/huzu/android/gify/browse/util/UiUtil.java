package com.huzu.android.gify.browse.util;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static android.view.inputmethod.InputMethodManager.SHOW_IMPLICIT;

/**
 * UI helper.
 */
public class UiUtil {

  private UiUtil() {
  }

  /**
   * Hides the soft keyboard.
   *
   * @param context the {@link Context}.
   * @param view    the {@link View}.
   */
  public static void hideKeyboard(Context context, View view) {
    InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
    if (imm != null) {
      imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
  }

  /**
   * Shows the soft keyboard.
   *
   * @param context the {@link Context}.
   * @param view    the {@link View}.
   */
  public static void showKeyboard(Context context, View view) {
    InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
    if (imm != null) {
      imm.showSoftInput(view, SHOW_IMPLICIT);
    }
  }
}
