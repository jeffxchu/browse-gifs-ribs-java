package com.huzu.android.gify.browse.ui;

import com.huzu.android.gify.browse.data.model.GifData;
import com.huzu.android.gify.browse.data.model.GifImage;

/**
 * Browse Gif View Model.
 */
public class BrowseGifViewModel {

  private final GifImage previewGif;

  private final GifImage originalGif;

  public BrowseGifViewModel(GifData data) {
    this.previewGif = data.images.previewGif;
    this.originalGif = data.images.originalGif;
  }

  GifImage getPreviewGif() {
    return previewGif;
  }

  GifImage getOriginalGif() {
    return originalGif;
  }
}
