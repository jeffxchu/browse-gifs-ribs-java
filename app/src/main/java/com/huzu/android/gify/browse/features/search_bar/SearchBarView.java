package com.huzu.android.gify.browse.features.search_bar;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.huzu.android.gify.browse.R;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;
import io.reactivex.Observable;

import static com.huzu.android.gify.browse.util.Constants.DEBOUNCE_SEARCH_BOX;

/**
 * Top level view for {@link SearchBarBuilder.SearchBarScope}.
 */
public class SearchBarView extends LinearLayout implements SearchBarInteractor.SearchBarPresenter {

  private EditText searchBox;

  public SearchBarView(Context context) {
    this(context, null);
  }

  public SearchBarView(Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public SearchBarView(Context context, @Nullable AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    searchBox = findViewById(R.id.editTextSearch);
  }

  @Override
  public Observable<String> queryText() {
    return RxTextView
        .textChanges(searchBox)
        .debounce(DEBOUNCE_SEARCH_BOX, TimeUnit.MILLISECONDS)
        .map(CharSequence::toString);

  }
}
