package com.huzu.android.gify.browse.root;

import com.huzu.android.gify.browse.features.search_bar.SearchBarBuilder;
import com.huzu.android.gify.browse.features.search_bar.SearchBarRouter;
import com.huzu.android.gify.browse.features.searching_gif.SearchingGifBuilder;
import com.huzu.android.gify.browse.features.searching_gif.SearchingGifRouter;
import com.huzu.android.gify.browse.features.trending_gif.TrendingGifBuilder;
import com.huzu.android.gify.browse.features.trending_gif.TrendingGifRouter;
import com.uber.rib.core.ViewRouter;

/**
 * Adds and removes children of {@link RootBuilder.RootScope}.
 */
public class RootRouter extends
    ViewRouter<RootView, RootInteractor, RootBuilder.Component> {

  private final TrendingGifBuilder trendingGifBuilder;
  private final SearchingGifBuilder searchingGifBuilder;
  private final SearchBarBuilder searchBarBuilder;

  private TrendingGifRouter trendingGifRouter;
  private SearchingGifRouter searchingGifRouter;
  private SearchBarRouter searchBarRouter;

  RootRouter(
      RootView view,
      RootInteractor interactor,
      RootBuilder.Component component,
      TrendingGifBuilder trendingGifBuilder,
      SearchingGifBuilder searchingGifBuilder,
      SearchBarBuilder searchBuilder) {
    super(view, interactor, component);
    this.trendingGifBuilder = trendingGifBuilder;
    this.searchingGifBuilder = searchingGifBuilder;
    this.searchBarBuilder = searchBuilder;
  }

  @Override
  protected void willDetach() {
    super.willDetach();
    detachTrendingGif();
    detachSearchingGif();
    detachSearchBar();
  }

  void attachSearchBar() {
    searchBarRouter = searchBarBuilder.build(getView().getTopContainer());
    attachChild(searchBarRouter);
    getView().getTopContainer().addView(searchBarRouter.getView());
  }

  void detachSearchBar() {
    if (searchBarRouter != null) {
      getView().getTopContainer().removeView(searchBarRouter.getView());
      detachChild(searchBarRouter);
      searchBarRouter = null;
    }
  }

  void attachTrendingGif() {
    if (trendingGifRouter == null) {
      trendingGifRouter = trendingGifBuilder.build(getView().getContentContainer());
      attachChild(trendingGifRouter);
      getView().getContentContainer().addView(trendingGifRouter.getView());
    }
  }

  void detachTrendingGif() {
    if (trendingGifRouter != null) {
      getView().getContentContainer().removeView(trendingGifRouter.getView());
      detachChild(trendingGifRouter);
      trendingGifRouter = null;
    }
  }

  void attachSearchingGif() {
    if (searchingGifRouter == null) {
      searchingGifRouter = searchingGifBuilder.build(getView().getContentContainer());
      attachChild(searchingGifRouter);
      getView().getContentContainer().addView(searchingGifRouter.getView());
    }
  }

  void detachSearchingGif() {
    if (searchingGifRouter != null) {
      getView().getContentContainer().removeView(searchingGifRouter.getView());
      detachChild(searchingGifRouter);
      searchingGifRouter = null;
    }
  }
}
