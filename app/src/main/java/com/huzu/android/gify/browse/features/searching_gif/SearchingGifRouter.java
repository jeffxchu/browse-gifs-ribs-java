package com.huzu.android.gify.browse.features.searching_gif;

import com.huzu.android.gify.browse.data.model.GifImage;
import com.huzu.android.gify.browse.features.gif_detail.GifDetailBuilder;
import com.huzu.android.gify.browse.features.gif_detail.GifDetailRouter;
import com.huzu.android.gify.browse.root.RootView;
import com.uber.rib.core.ViewRouter;

/**
 * Adds and removes children of {@link SearchingGifBuilder.SearchingGifScope}.
 */
public class SearchingGifRouter extends
    ViewRouter<SearchingGifView, SearchingGifInteractor, SearchingGifBuilder.Component> {

  private final GifDetailBuilder gifDetailBuilder;
  private final RootView rootView;

  private GifDetailRouter gifDetailRouter;

  SearchingGifRouter(
      SearchingGifView view,
      RootView rootView,
      SearchingGifInteractor interactor,
      SearchingGifBuilder.Component component,
      GifDetailBuilder gifDetailBuilder) {
    super(view, interactor, component);
    this.rootView = rootView;
    this.gifDetailBuilder = gifDetailBuilder;
  }

  @Override
  protected void willDetach() {
    super.willDetach();
    detachGifDetail();
  }

  void attachGifDetail(GifImage gifImage) {
    gifDetailRouter = gifDetailBuilder.build(gifImage, rootView);
    attachChild(gifDetailRouter);
    rootView.addView(gifDetailRouter.getView());
  }

  void detachGifDetail() {
    if (gifDetailRouter != null) {
      rootView.removeView(gifDetailRouter.getView());
      detachChild(gifDetailRouter);
      gifDetailRouter = null;
    }
  }
}
