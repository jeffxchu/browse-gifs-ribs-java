package com.huzu.android.gify.browse.features.search_bar;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.huzu.android.gify.browse.R;
import com.huzu.android.gify.browse.util.SearchQueryStream;
import com.uber.rib.core.InteractorBaseComponent;
import com.uber.rib.core.ViewBuilder;

import java.lang.annotation.Retention;

import javax.inject.Qualifier;
import javax.inject.Scope;

import dagger.Binds;
import dagger.BindsInstance;
import dagger.Provides;

import static java.lang.annotation.RetentionPolicy.CLASS;

/**
 * Builder for the {@link SearchBarScope}.
 */
public class SearchBarBuilder
    extends ViewBuilder<SearchBarView, SearchBarRouter, SearchBarBuilder.ParentComponent> {

  public SearchBarBuilder(ParentComponent dependency) {
    super(dependency);
  }

  /**
   * Builds a new {@link SearchBarRouter}.
   *
   * @param parentViewGroup parent view group that this router's view will be added to.
   * @return a new {@link SearchBarRouter}.
   */
  public SearchBarRouter build(ViewGroup parentViewGroup) {
    SearchBarView view = createView(parentViewGroup);
    SearchBarInteractor interactor = new SearchBarInteractor();
    Component component = DaggerSearchBarBuilder_Component.builder()
        .parentComponent(getDependency())
        .view(view)
        .interactor(interactor)
        .build();
    return component.searchbarRouter();
  }

  @Override
  protected SearchBarView inflateView(LayoutInflater inflater, ViewGroup parentViewGroup) {
    return (SearchBarView) inflater.inflate(R.layout.rib_search_bar, parentViewGroup, false);
  }

  public interface ParentComponent {

    SearchQueryStream searchQueryStream();
  }

  @dagger.Module
  public abstract static class Module {

    @SearchBarScope
    @Binds
    abstract SearchBarInteractor.SearchBarPresenter presenter(SearchBarView view);

    @SearchBarScope
    @Provides
    static SearchBarRouter router(
        Component component,
        SearchBarView view,
        SearchBarInteractor interactor) {
      return new SearchBarRouter(view, interactor, component);
    }
  }

  @SearchBarScope
  @dagger.Component(modules = Module.class,
      dependencies = ParentComponent.class)
  interface Component extends
      InteractorBaseComponent<SearchBarInteractor>,
      BuilderComponent {

    @dagger.Component.Builder
    interface Builder {
      @BindsInstance
      Builder interactor(SearchBarInteractor interactor);

      @BindsInstance
      Builder view(SearchBarView view);

      Builder parentComponent(ParentComponent component);

      Component build();
    }
  }

  interface BuilderComponent {
    SearchBarRouter searchbarRouter();
  }

  @Scope
  @Retention(CLASS)
  @interface SearchBarScope {
  }

  @Qualifier
  @Retention(CLASS)
  @interface SearchBarInternal {
  }
}
