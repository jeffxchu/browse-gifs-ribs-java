package com.huzu.android.gify.browse.data.model;

import com.squareup.moshi.Json;

public final class Pagination {

  @Json(name = "total_count")
  public int totalCount;

  public int count;

  public int offset;
}
