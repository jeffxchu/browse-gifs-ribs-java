package com.huzu.android.gify.browse.features.searching_gif;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.huzu.android.gify.browse.R;
import com.huzu.android.gify.browse.data.repository.GiphyRepository;
import com.huzu.android.gify.browse.features.gif_detail.GifDetailBuilder;
import com.huzu.android.gify.browse.features.gif_detail.GifDetailInteractor;
import com.huzu.android.gify.browse.root.RootView;
import com.huzu.android.gify.browse.util.SearchQueryStream;
import com.uber.rib.core.ActivityContext;
import com.uber.rib.core.InteractorBaseComponent;
import com.uber.rib.core.ViewBuilder;

import java.lang.annotation.Retention;

import javax.inject.Qualifier;
import javax.inject.Scope;

import dagger.Binds;
import dagger.BindsInstance;
import dagger.Provides;

import static java.lang.annotation.RetentionPolicy.CLASS;

/**
 * Builder for the {@link SearchingGifScope}.
 */
public class SearchingGifBuilder
    extends ViewBuilder<SearchingGifView, SearchingGifRouter, SearchingGifBuilder.ParentComponent> {

  public SearchingGifBuilder(ParentComponent dependency) {
    super(dependency);
  }

  /**
   * Builds a new {@link SearchingGifRouter}.
   *
   * @param parentViewGroup parent view group that this router's view will be added to.
   * @return a new {@link SearchingGifRouter}.
   */
  public SearchingGifRouter build(ViewGroup parentViewGroup) {
    SearchingGifView view = createView(parentViewGroup);
    SearchingGifInteractor interactor = new SearchingGifInteractor();
    Component component = DaggerSearchingGifBuilder_Component.builder()
        .parentComponent(getDependency())
        .view(view)
        .interactor(interactor)
        .build();
    return component.searchinggifRouter();
  }

  @Override
  protected SearchingGifView inflateView(LayoutInflater inflater, ViewGroup parentViewGroup) {
    return (SearchingGifView) inflater.inflate(R.layout.rib_search_result, parentViewGroup, false);
  }

  public interface ParentComponent {

    @ActivityContext
    Context context();

    GiphyRepository giphyRepository();

    RootView rootView();

    SearchQueryStream searchQueryStream();
  }

  @dagger.Module
  public abstract static class Module {

    @SearchingGifScope
    @Binds
    abstract SearchingGifInteractor.SearchingGifPresenter presenter(SearchingGifView view);

    @SearchingGifScope
    @Provides
    static SearchingGifRouter router(
        Component component,
        SearchingGifView view,
        RootView rootView,
        SearchingGifInteractor interactor) {
      return new SearchingGifRouter(
          view,
          rootView,
          interactor,
          component,
          new GifDetailBuilder(component));
    }

    @SearchingGifScope
    @Provides
    static GifDetailInteractor.Listener provideGifDetailListener(SearchingGifInteractor interactor) {
      return interactor.new GifDetailListener();
    }
  }

  @SearchingGifScope
  @dagger.Component(modules = Module.class,
      dependencies = ParentComponent.class)
  interface Component extends
      InteractorBaseComponent<SearchingGifInteractor>,
      BuilderComponent,
      GifDetailBuilder.ParentComponent {

    @dagger.Component.Builder
    interface Builder {
      @BindsInstance
      Builder interactor(SearchingGifInteractor interactor);

      @BindsInstance
      Builder view(SearchingGifView view);

      Builder parentComponent(ParentComponent component);

      Component build();
    }
  }

  interface BuilderComponent {
    SearchingGifRouter searchinggifRouter();
  }

  @Scope
  @Retention(CLASS)
  @interface SearchingGifScope {
  }

  @Qualifier
  @Retention(CLASS)
  @interface SearchingGifInternal {
  }
}
