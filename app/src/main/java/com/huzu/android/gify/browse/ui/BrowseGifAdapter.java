package com.huzu.android.gify.browse.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.huzu.android.gify.browse.R;
import com.huzu.android.gify.browse.data.model.GifImage;
import com.jakewharton.rxbinding2.view.RxView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

import static com.huzu.android.gify.browse.util.Constants.DEFAULT_THROTTLE;
import static com.huzu.android.gify.browse.util.Constants.TRENDING_GIF_LIMIT;

/**
 * A {@link RecyclerView.Adapter} renders a gif. It also supports the paging data.
 */
public class BrowseGifAdapter extends RecyclerView.Adapter<BrowseGifAdapter.ViewHolder> {

  private final List<BrowseGifViewModel> gifImages = new ArrayList<>();
  private final PublishSubject<GifImage> imageClicks = PublishSubject.create();

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(
        R.layout.row_image,
        parent,
        false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {
    holder.bind(gifImages.get(position));
  }

  @Override
  public int getItemCount() {
    return gifImages.size();
  }

  /**
   * @return an {@link Observable} of an image clicks.
   */
  public Observable<GifImage> imageClicks() {
    return imageClicks.hide();
  }

  /**
   * Clears the gif collection.
   */
  public void clear() {
    gifImages.clear();
    notifyDataSetChanged();
  }

  /**
   * Updates the gif collection with paging.
   *
   * @param images a list of {@link BrowseGifViewModel}.
   * @param page   the page offset e.g. 0, 1, 2, 3 etc.
   */
  public void updatePagedData(List<BrowseGifViewModel> images, int page) {
    gifImages.addAll(images);
    notifyItemRangeInserted(page * TRENDING_GIF_LIMIT, TRENDING_GIF_LIMIT);
  }

  /**
   * Sets the gif collection to the adapter. It clears the current data collection.
   *
   * @param images a list of {@link BrowseGifViewModel}.
   */
  public void setData(List<BrowseGifViewModel> images) {
    gifImages.clear();
    gifImages.addAll(images);
    notifyDataSetChanged();
  }

  /**
   * A {@link ViewHolder} binds a {@link GifImage}.
   */
  class ViewHolder extends RecyclerView.ViewHolder {

    ViewHolder(View view) {
      super(view);
    }

    void bind(BrowseGifViewModel viewModel) {
      GiphyImageView imageView = itemView.findViewById(R.id.imageViewGif);
      GifImage image = viewModel.getPreviewGif();
      float ratio = (float) image.height / image.width;
      imageView.setRatio(ratio);

      Glide
          .with(imageView.getContext())
          .asGif()
          .load(image.url)
          .placeholder(R.drawable.placeholder_gif)
          .into(imageView);

      RxView
          .clicks(imageView)
          .throttleFirst(DEFAULT_THROTTLE, TimeUnit.MILLISECONDS)
          .map(__ -> viewModel.getOriginalGif())
          .subscribe(imageClicks);
    }
  }
}
