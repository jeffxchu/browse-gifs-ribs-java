package com.huzu.android.gify.browse.features.trending_gif;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.huzu.android.gify.browse.R;
import com.huzu.android.gify.browse.data.repository.GiphyRepository;
import com.huzu.android.gify.browse.features.gif_detail.GifDetailBuilder;
import com.huzu.android.gify.browse.features.gif_detail.GifDetailInteractor;
import com.huzu.android.gify.browse.root.RootView;
import com.uber.rib.core.ActivityContext;
import com.uber.rib.core.InteractorBaseComponent;
import com.uber.rib.core.ViewBuilder;

import java.lang.annotation.Retention;

import javax.inject.Qualifier;
import javax.inject.Scope;

import dagger.Binds;
import dagger.BindsInstance;
import dagger.Provides;

import static java.lang.annotation.RetentionPolicy.CLASS;

/**
 * Builder for the {@link TrendingGifScope}.
 */
public class TrendingGifBuilder
    extends ViewBuilder<TrendingGifView, TrendingGifRouter, TrendingGifBuilder.ParentComponent> {

  public TrendingGifBuilder(ParentComponent dependency) {
    super(dependency);
  }

  /**
   * Builds a new {@link TrendingGifRouter}.
   *
   * @param parentViewGroup parent view group that this router's view will be added to.
   * @return a new {@link TrendingGifRouter}.
   */
  public TrendingGifRouter build(ViewGroup parentViewGroup) {
    TrendingGifView view = createView(parentViewGroup);
    TrendingGifInteractor interactor = new TrendingGifInteractor();
    Component component = DaggerTrendingGifBuilder_Component.builder()
        .parentComponent(getDependency())
        .view(view)
        .interactor(interactor)
        .build();
    return component.trendinggifRouter();
  }

  @Override
  protected TrendingGifView inflateView(LayoutInflater inflater, ViewGroup parentViewGroup) {
    return (TrendingGifView) inflater.inflate(R.layout.rib_trending, parentViewGroup, false);
  }

  public interface ParentComponent {

    @ActivityContext
    Context context();

    GiphyRepository giphyRepository();

    RootView rootView();
  }

  @dagger.Module
  public abstract static class Module {

    @TrendingGifScope
    @Binds
    abstract TrendingGifInteractor.TrendingGifPresenter presenter(TrendingGifView view);

    @TrendingGifScope
    @Provides
    static TrendingGifRouter router(
        Component component,
        TrendingGifView view,
        RootView rootView,
        TrendingGifInteractor interactor) {
      return new TrendingGifRouter(view, rootView, interactor, component, new GifDetailBuilder(component));
    }

    @TrendingGifScope
    @Provides
    static GifDetailInteractor.Listener provideGifDetailListener(TrendingGifInteractor interactor) {
      return interactor.new GifDetailListener();
    }
  }

  @TrendingGifScope
  @dagger.Component(modules = Module.class,
      dependencies = ParentComponent.class)
  interface Component extends
      InteractorBaseComponent<TrendingGifInteractor>,
      BuilderComponent,
      GifDetailBuilder.ParentComponent {

    @dagger.Component.Builder
    interface Builder {
      @BindsInstance
      Builder interactor(TrendingGifInteractor interactor);

      @BindsInstance
      Builder view(TrendingGifView view);

      Builder parentComponent(ParentComponent component);

      Component build();
    }
  }

  interface BuilderComponent {
    TrendingGifRouter trendinggifRouter();
  }

  @Scope
  @Retention(CLASS)
  @interface TrendingGifScope {
  }

  @Qualifier
  @Retention(CLASS)
  @interface TrendingGifInternal {
  }
}
