package com.huzu.android.gify.browse.features.trending_gif;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.huzu.android.gify.browse.R;
import com.huzu.android.gify.browse.data.model.GifImage;
import com.huzu.android.gify.browse.ui.BrowseGifAdapter;
import com.huzu.android.gify.browse.ui.BrowseGifViewModel;
import com.huzu.android.gify.browse.ui.GridSpacingItemDecoration;
import com.huzu.android.gify.browse.util.UiUtil;
import com.jakewharton.rxbinding2.view.RxView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE;
import static com.huzu.android.gify.browse.util.Constants.DEFAULT_THROTTLE;

/**
 * Top level view for {@link TrendingGifBuilder.TrendingGifScope}.
 */
public class TrendingGifView extends FrameLayout
    implements TrendingGifInteractor.TrendingGifPresenter {

  private static final int PAGING_THRESHOLD = 10;

  private final PublishSubject<Boolean> loadMoreSubject = PublishSubject.create();
  private final BrowseGifAdapter adapter = new BrowseGifAdapter();

  private View loadingView;
  private RecyclerView recyclerView;
  private FloatingActionButton fabHome;
  private SwipeRefreshLayout refreshLayout;
  private boolean loading;

  private int lastVisibleItem;
  private int totalItemsCount;
  private int[] lastPositions;

  public TrendingGifView(Context context) {
    this(context, null);
  }

  public TrendingGifView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    recyclerView = findViewById(R.id.recyclerViewGif);
    recyclerView.setAdapter(adapter);
    int spacing = getResources().getDimensionPixelSize(R.dimen.default_space_half);
    recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, spacing, true));

    loadingView = findViewById(R.id.loadingView);
    fabHome = findViewById(R.id.fabHome);

    refreshLayout = findViewById(R.id.refreshLayout);
    refreshLayout.setColorSchemeColors(
        getResources().getColor(android.R.color.holo_blue_bright),
        getResources().getColor(android.R.color.holo_green_light),
        getResources().getColor(android.R.color.holo_orange_light),
        getResources().getColor(android.R.color.holo_red_light)
    );

    StaggeredGridLayoutManager layoutManager = (StaggeredGridLayoutManager) recyclerView.getLayoutManager();

    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        if (newState == SCROLL_STATE_IDLE) {
          showHomeFab();
        } else {
          hideHomeFab();
        }
      }

      @Override
      public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (dy > 0) {
          totalItemsCount = layoutManager.getItemCount();
          if (lastPositions == null) {
            lastPositions = new int[layoutManager.getSpanCount()];
          }
          lastPositions = layoutManager.findLastCompletelyVisibleItemPositions(lastPositions);
          lastVisibleItem = Math.max(lastPositions[0], lastPositions[1]);
          if (!loading && (lastVisibleItem + PAGING_THRESHOLD) >= totalItemsCount) {
            loadMoreSubject.onNext(true);
          }
          UiUtil.hideKeyboard(getContext(), recyclerView);
        }
      }
    });
  }

  @Override
  public void updateTrendingGifs(List<BrowseGifViewModel> data, int offset) {
    adapter.updatePagedData(data, offset);
  }

  @Override
  public void showLoadingView() {
    loadingView.setVisibility(VISIBLE);
    loading = true;
  }

  @Override
  public void hideLoadingView() {
    loadingView.setVisibility(GONE);
    loading = false;
  }

  @Override
  public void showHomeFab() {
    if (fabHome.getVisibility() == GONE) {
      fabHome.setVisibility(VISIBLE);
    }
  }

  @Override
  public void hideHomeFab() {
    if (fabHome.getVisibility() == VISIBLE) {
      fabHome.setVisibility(GONE);
    }
  }

  @Override
  public void setSwipeRefreshListener(SwipeRefreshLayout.OnRefreshListener listener) {
    refreshLayout.setOnRefreshListener(listener);
  }

  @Override
  public void hideRefreshing() {
    if (refreshLayout.isRefreshing()) {
      refreshLayout.setRefreshing(false);
    }
  }

  @Override
  public void cleanup() {
    adapter.clear();
  }

  @Override
  public void scrollToTop() {
    recyclerView.smoothScrollToPosition(0);
  }

  @Override
  public Observable<Boolean> loadMore() {
    return loadMoreSubject.hide();
  }

  @Override
  public Observable<Object> bringToTopClicks() {
    return RxView.clicks(fabHome);
  }

  @Override
  public Observable<GifImage> gifClicks() {
    return adapter
        .imageClicks()
        .throttleFirst(DEFAULT_THROTTLE, TimeUnit.MILLISECONDS)
        .doOnNext(__ -> UiUtil.hideKeyboard(getContext(), recyclerView));
  }
}
