package com.huzu.android.gify.browse.features.search_bar;

import com.huzu.android.gify.browse.util.SearchQueryStream;
import com.uber.rib.core.Bundle;
import com.uber.rib.core.Interactor;
import com.uber.rib.core.RibInteractor;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import io.reactivex.Observable;

import static com.uber.autodispose.AutoDispose.autoDisposable;
import static io.reactivex.android.schedulers.AndroidSchedulers.mainThread;
import static io.reactivex.schedulers.Schedulers.io;

/**
 * Coordinates Business Logic for {@link SearchBarBuilder.SearchBarScope}.
 *
 * A search bar handles the user query text and updates {@link SearchQueryStream}.
 */
@RibInteractor
public class SearchBarInteractor
    extends Interactor<SearchBarInteractor.SearchBarPresenter, SearchBarRouter> {

  @Inject
  SearchBarPresenter presenter;

  @Inject
  SearchQueryStream searchQueryStream;

  @Override
  protected void didBecomeActive(@Nullable Bundle savedInstanceState) {
    super.didBecomeActive(savedInstanceState);

    presenter
        .queryText()
        .subscribeOn(io())
        .observeOn(mainThread())
        .as(autoDisposable(this))
        .subscribe(query -> searchQueryStream.updateQueryText(query));
  }

  @Override
  protected void willResignActive() {
    super.willResignActive();
  }

  /**
   * Presenter interface implemented by this RIB's view.
   */
  interface SearchBarPresenter {

    Observable<String> queryText();
  }
}
