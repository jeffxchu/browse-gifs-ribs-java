package com.huzu.android.gify.browse.features.gif_detail;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.huzu.android.gify.browse.R;
import com.huzu.android.gify.browse.data.model.GifImage;
import com.uber.rib.core.InteractorBaseComponent;
import com.uber.rib.core.ViewBuilder;

import java.lang.annotation.Retention;

import javax.inject.Qualifier;
import javax.inject.Scope;

import dagger.Binds;
import dagger.BindsInstance;
import dagger.Provides;

import static java.lang.annotation.RetentionPolicy.CLASS;

/**
 * Builder for the {@link GifDetailScope}.
 */
public class GifDetailBuilder
    extends ViewBuilder<GifDetailView, GifDetailRouter, GifDetailBuilder.ParentComponent> {

  public GifDetailBuilder(ParentComponent dependency) {
    super(dependency);
  }

  /**
   * Builds a new {@link GifDetailRouter}.
   *
   * @param gifImage        the {@link GifImage}.
   * @param parentViewGroup parent view group that this router's view will be added to.
   * @return a new {@link GifDetailRouter}.
   */
  public GifDetailRouter build(GifImage gifImage, ViewGroup parentViewGroup) {
    GifDetailView view = createView(parentViewGroup);
    GifDetailInteractor interactor = new GifDetailInteractor();
    Component component = DaggerGifDetailBuilder_Component.builder()
        .parentComponent(getDependency())
        .view(view)
        .trendingImage(gifImage)
        .interactor(interactor)
        .build();
    return component.gifdetailRouter();
  }

  @Override
  protected GifDetailView inflateView(LayoutInflater inflater, ViewGroup parentViewGroup) {
    return (GifDetailView) inflater.inflate(R.layout.rib_gif_detail, parentViewGroup, false);
  }

  public interface ParentComponent {
    GifDetailInteractor.Listener gifDetailListener();
  }

  @dagger.Module
  public abstract static class Module {

    @GifDetailScope
    @Binds
    abstract GifDetailInteractor.GifDetailPresenter presenter(GifDetailView view);

    @GifDetailScope
    @Provides
    static GifDetailRouter router(
        Component component,
        GifDetailView view,
        GifDetailInteractor interactor) {
      return new GifDetailRouter(view, interactor, component);
    }
  }

  @GifDetailScope
  @dagger.Component(modules = Module.class,
      dependencies = ParentComponent.class)
  interface Component extends InteractorBaseComponent<GifDetailInteractor>, BuilderComponent {

    @dagger.Component.Builder
    interface Builder {
      @BindsInstance
      Builder interactor(GifDetailInteractor interactor);

      @BindsInstance
      Builder view(GifDetailView view);

      @BindsInstance
      Builder trendingImage(GifImage gifImage);

      Builder parentComponent(ParentComponent component);

      Component build();
    }
  }

  interface BuilderComponent {
    GifDetailRouter gifdetailRouter();
  }

  @Scope
  @Retention(CLASS)
  @interface GifDetailScope {
  }

  @Qualifier
  @Retention(CLASS)
  @interface GifDetailInternal {
  }
}
