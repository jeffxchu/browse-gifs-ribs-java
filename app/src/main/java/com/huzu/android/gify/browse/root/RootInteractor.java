package com.huzu.android.gify.browse.root;

import com.huzu.android.gify.browse.util.SearchQueryStream;
import com.uber.rib.core.Bundle;
import com.uber.rib.core.Interactor;
import com.uber.rib.core.RibInteractor;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import timber.log.Timber;

import static com.uber.autodispose.AutoDispose.autoDisposable;
import static io.reactivex.android.schedulers.AndroidSchedulers.mainThread;

/**
 * Coordinates Business Logic for {@link RootBuilder.RootScope}.
 */
@RibInteractor
public class RootInteractor
    extends Interactor<RootInteractor.RootPresenter, RootRouter> {

  @Inject
  RootPresenter presenter;

  @Inject
  SearchQueryStream searchQueryStream;

  @Override
  protected void didBecomeActive(@Nullable Bundle savedInstanceState) {
    super.didBecomeActive(savedInstanceState);
    getRouter().attachSearchBar();
    subscribeSearchQueryStream();
  }

  private void subscribeSearchQueryStream() {
    searchQueryStream
        .queryObservable()
        .distinctUntilChanged()
        .map(query -> query == null || query.length() == 0)
        .observeOn(mainThread())
        .as(autoDisposable(this))
        .subscribe(isTrending -> {
          if (isTrending) {
            getRouter().detachSearchingGif();
            getRouter().attachTrendingGif();
          } else {
            getRouter().detachTrendingGif();
            getRouter().attachSearchingGif();
          }
        }, Timber::e);
  }

  @Override
  protected void willResignActive() {
    super.willResignActive();
    getRouter().detachSearchBar();
    getRouter().detachTrendingGif();
    getRouter().detachSearchingGif();
  }

  /**
   * Presenter interface implemented by this RIB's view.
   */
  interface RootPresenter {
  }
}
