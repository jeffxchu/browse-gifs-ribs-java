package com.huzu.android.gify.browse.data.repository;

import com.huzu.android.gify.browse.data.GiphyApi;
import com.huzu.android.gify.browse.data.model.GifData;

import java.util.List;

import io.reactivex.Observable;

import static com.huzu.android.gify.browse.data.GiphyApi.API_KEY;
import static com.huzu.android.gify.browse.util.Constants.RATING_G;
import static com.huzu.android.gify.browse.util.Constants.SEARCHING_GIF_LIMIT;
import static com.huzu.android.gify.browse.util.Constants.TRENDING_GIF_LIMIT;

/**
 * The giphy repository.
 */
public class GiphyRepository {

  private final GiphyApi giphyApi;

  public GiphyRepository(GiphyApi giphyApi) {
    this.giphyApi = giphyApi;
  }

  /**
   * A trending gifs observable.
   *
   * @param page the pagination offset.
   * @return an {@link Observable} of trending gifs.
   */
  public Observable<List<GifData>> trendingObservable(int page) {
    return giphyApi
        .getTrending(API_KEY, TRENDING_GIF_LIMIT, page * TRENDING_GIF_LIMIT, RATING_G)
        .map(list -> list.data);
  }

  /**
   * A search gif result observable.
   *
   * @param query the query text.
   * @return an {@link Observable} of trending gifs.
   */
  public Observable<List<GifData>> searchObservable(String query) {
    return giphyApi
        .search(API_KEY, query, SEARCHING_GIF_LIMIT, RATING_G)
        .map(list -> list.data);
  }
}
