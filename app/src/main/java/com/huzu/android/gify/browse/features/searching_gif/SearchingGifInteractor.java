package com.huzu.android.gify.browse.features.searching_gif;

import com.huzu.android.gify.browse.data.model.GifImage;
import com.huzu.android.gify.browse.data.repository.GiphyRepository;
import com.huzu.android.gify.browse.features.gif_detail.GifDetailInteractor;
import com.huzu.android.gify.browse.ui.BrowseGifViewModel;
import com.huzu.android.gify.browse.util.SearchQueryStream;
import com.uber.rib.core.Bundle;
import com.uber.rib.core.Interactor;
import com.uber.rib.core.RibInteractor;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import io.reactivex.Observable;
import timber.log.Timber;

import static com.uber.autodispose.AutoDispose.autoDisposable;
import static io.reactivex.android.schedulers.AndroidSchedulers.mainThread;
import static io.reactivex.schedulers.Schedulers.io;

/**
 * Coordinates Business Logic for {@link SearchingGifBuilder.SearchingGifScope}.
 *
 * The interactor handles the gif search result via {@link SearchQueryStream}.
 */
@RibInteractor
public class SearchingGifInteractor
    extends Interactor<SearchingGifInteractor.SearchingGifPresenter, SearchingGifRouter> {

  @Inject
  SearchingGifPresenter presenter;

  @Inject
  GiphyRepository giphyRepository;

  @Inject
  SearchQueryStream searchQueryStream;

  @Override
  protected void didBecomeActive(@Nullable Bundle savedInstanceState) {
    super.didBecomeActive(savedInstanceState);
    subscribeSearchResult();
    subscribeGifClicks();
  }

  private void subscribeGifClicks() {
    presenter
        .gifClicks()
        .observeOn(mainThread())
        .as(autoDisposable(this))
        .subscribe(gif -> getRouter().attachGifDetail(gif));
  }

  @Override
  protected void willResignActive() {
    super.willResignActive();
  }

  private void subscribeSearchResult() {
    searchQueryStream
        .queryObservable()
        .distinctUntilChanged()
        .switchMap(query ->
            giphyRepository
                .searchObservable(query)
                .flatMapIterable(list -> list)
                .map(BrowseGifViewModel::new)
                .toList()
                .toObservable()
                .subscribeOn(io())
        )
        .observeOn(mainThread())
        .doOnSubscribe(__ -> presenter.showLoadingView())
        .as(autoDisposable(this))
        .subscribe(data -> {
          presenter.setData(data);
          presenter.hideLoadingView();
          if (data.size() == 0) {
            presenter.showEmptyView();
          } else {
            presenter.hideEmptyView();
          }
        }, Timber::e);
  }

  public class GifDetailListener implements GifDetailInteractor.Listener {

    @Override
    public void dismiss() {
      getRouter().detachGifDetail();
    }
  }

  /**
   * Presenter interface implemented by this RIB's view.
   */
  interface SearchingGifPresenter {

    void setData(List<BrowseGifViewModel> data);

    void showLoadingView();

    void hideLoadingView();

    void showEmptyView();

    void hideEmptyView();

    Observable<GifImage> gifClicks();
  }
}
