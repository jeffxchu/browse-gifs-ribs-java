package com.huzu.android.gify.browse.util;

/**
 * App constants.
 */
public final class Constants {

  public static final long DEFAULT_THROTTLE = 475L;
  public static final long DEBOUNCE_SEARCH_BOX = 475L;

  public static final int TRENDING_GIF_LIMIT = 30;
  public static final int SEARCHING_GIF_LIMIT = 50;

  public static final String RATING_G = "G";
}