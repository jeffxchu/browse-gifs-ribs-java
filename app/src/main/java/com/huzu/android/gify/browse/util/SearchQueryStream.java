package com.huzu.android.gify.browse.util;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * A data stream for the search query.
 */
public class SearchQueryStream {

  private PublishSubject<String> queryTextSubject = PublishSubject.create();

  /**
   * Updates the query text.
   *
   * @param query a query text.
   */
  public void updateQueryText(String query) {
    queryTextSubject.onNext(query);
  }

  /**
   * A query observable.
   *
   * @return an {@link Observable} of query text.
   */
  public Observable<String> queryObservable() {
    return queryTextSubject.hide();
  }
}
