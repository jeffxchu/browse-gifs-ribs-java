package com.huzu.android.gify.browse.di.module;

import android.app.Application;

import com.huzu.android.gify.browse.data.GiphyApi;
import com.squareup.moshi.Moshi;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

@Module
public class NetworkModule {

  private static final int HTTP_TIMEOUT_MS = 10 * 1000;

  @Provides
  GiphyApi provideGiphyApi(Retrofit retrofit) {
    return retrofit.create(GiphyApi.class);
  }

  @Provides
  Cache provideOkHttpCache(Application application) {
    int cacheSize = 10 * 1024 * 1024; // 10 MiB
    return new Cache(application.getCacheDir(), cacheSize);
  }

  @Provides
  Moshi provideMoshi() {
    return new Moshi.Builder().build();
  }

  @Provides
  OkHttpClient provideOkHttpClient(Cache cache) {
    return new OkHttpClient.Builder()
        .connectTimeout(HTTP_TIMEOUT_MS, MILLISECONDS)
        .readTimeout(HTTP_TIMEOUT_MS, MILLISECONDS)
        .writeTimeout(HTTP_TIMEOUT_MS, MILLISECONDS)
        .cache(cache)
        .build();
  }

  @Provides
  Retrofit provideRetrofit(Moshi moshi, OkHttpClient okHttpClient) {
    return new Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl(GiphyApi.BASE_URL)
        .client(okHttpClient)
        .build();
  }
}
