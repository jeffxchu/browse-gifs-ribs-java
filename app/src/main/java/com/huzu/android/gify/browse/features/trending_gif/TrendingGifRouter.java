package com.huzu.android.gify.browse.features.trending_gif;

import com.huzu.android.gify.browse.data.model.GifImage;
import com.huzu.android.gify.browse.features.gif_detail.GifDetailBuilder;
import com.huzu.android.gify.browse.features.gif_detail.GifDetailRouter;
import com.huzu.android.gify.browse.root.RootView;
import com.uber.rib.core.ViewRouter;

/**
 * Adds and removes children of {@link TrendingGifBuilder.TrendingGifScope}.
 */
public class TrendingGifRouter extends
    ViewRouter<TrendingGifView, TrendingGifInteractor, TrendingGifBuilder.Component> {

  private final GifDetailBuilder gifDetailBuilder;
  private final RootView rootView;

  private GifDetailRouter gifDetailRouter;

  public TrendingGifRouter(
      TrendingGifView view,
      RootView rootView,
      TrendingGifInteractor interactor,
      TrendingGifBuilder.Component component,
      GifDetailBuilder gifDetailBuilder) {
    super(view, interactor, component);
    this.gifDetailBuilder = gifDetailBuilder;
    this.rootView = rootView;
  }

  @Override
  protected void willDetach() {
    super.willDetach();
    detachGifDetail();
  }

  void attachGifDetail(GifImage gifImage) {
    gifDetailRouter = gifDetailBuilder.build(gifImage, rootView);
    attachChild(gifDetailRouter);
    rootView.addView(gifDetailRouter.getView());
  }

  void detachGifDetail() {
    if (gifDetailRouter != null) {
      rootView.removeView(gifDetailRouter.getView());
      detachChild(gifDetailRouter);
      gifDetailRouter = null;
    }
  }
}
