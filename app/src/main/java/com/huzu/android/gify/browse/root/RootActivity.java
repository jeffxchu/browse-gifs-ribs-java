package com.huzu.android.gify.browse.root;

import android.os.Bundle;
import android.view.ViewGroup;

import com.uber.rib.core.RibActivity;
import com.uber.rib.core.ViewRouter;

import dagger.android.AndroidInjection;

/**
 * RootActivity is the single {@link RibActivity} for the app.
 */
public class RootActivity extends RibActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    AndroidInjection.inject(this);
    super.onCreate(savedInstanceState);
  }

  @Override
  protected ViewRouter<?, ?, ?> createRouter(ViewGroup parentViewGroup) {
    RootBuilder rootBuilder = new RootBuilder(new RootBuilder.ParentComponent() {
    });
    return rootBuilder.build(getApplication(), parentViewGroup);
  }
}
