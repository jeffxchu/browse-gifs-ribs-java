package com.huzu.android.gify.browse.features.trending_gif;

import android.content.Context;

import com.huzu.android.gify.browse.data.model.GifImage;
import com.huzu.android.gify.browse.data.repository.GiphyRepository;
import com.huzu.android.gify.browse.features.gif_detail.GifDetailInteractor;
import com.huzu.android.gify.browse.ui.BrowseGifViewModel;
import com.uber.rib.core.ActivityContext;
import com.uber.rib.core.Bundle;
import com.uber.rib.core.Interactor;
import com.uber.rib.core.RibInteractor;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import io.reactivex.Observable;
import timber.log.Timber;

import static com.uber.autodispose.AutoDispose.autoDisposable;
import static io.reactivex.android.schedulers.AndroidSchedulers.mainThread;
import static io.reactivex.schedulers.Schedulers.io;

/**
 * Coordinates Business Logic for {@link TrendingGifBuilder.TrendingGifScope}.
 *
 * The interactor handles a list of trending gifs.
 */
@RibInteractor
public class TrendingGifInteractor
    extends Interactor<TrendingGifInteractor.TrendingGifPresenter, TrendingGifRouter>
    implements SwipeRefreshLayout.OnRefreshListener {

  @Inject
  @ActivityContext
  Context context;

  @Inject
  TrendingGifPresenter presenter;

  @Inject
  GiphyRepository giphyRepository;

  private int page;

  @Override
  protected void didBecomeActive(@Nullable Bundle savedInstanceState) {
    super.didBecomeActive(savedInstanceState);
    presenter.setSwipeRefreshListener(this);
    onRefresh();
    subscribeLoadMore();
    subscribeBringToTopClicks();
    subscribeGifClicks();
  }

  private void subscribeGifClicks() {
    presenter
        .gifClicks()
        .observeOn(mainThread())
        .as(autoDisposable(this))
        .subscribe(trendingImage -> getRouter().attachGifDetail(trendingImage));
  }

  private void subscribeBringToTopClicks() {
    presenter
        .bringToTopClicks()
        .observeOn(mainThread())
        .as(autoDisposable(this))
        .subscribe(__ -> presenter.scrollToTop());
  }

  private void subscribeLoadMore() {
    presenter
        .loadMore()
        .map(__ -> ++page)
        .observeOn(mainThread())
        .as(autoDisposable(this))
        .subscribe(this::fetchGifs);
  }

  @Override
  protected void willResignActive() {
    super.willResignActive();
  }

  @Override
  public void onRefresh() {
    presenter.cleanup();
    page = 0;
    fetchGifs(page);
  }

  void fetchGifs(int page) {
    Timber.d("offset = %s", page);
    giphyRepository
        .trendingObservable(page)
        .flatMapIterable(list -> list)
        .map(BrowseGifViewModel::new)
        .toList()
        .subscribeOn(io())
        .observeOn(mainThread())
        .doOnSubscribe(__ -> presenter.showLoadingView())
        .doFinally(() -> {
          presenter.hideRefreshing();
          presenter.hideLoadingView();
        })
        .as(autoDisposable(this))
        .subscribe(
            data -> presenter.updateTrendingGifs(data, page),
            Timber::e
        );
  }

  class GifDetailListener implements GifDetailInteractor.Listener {
    @Override
    public void dismiss() {
      getRouter().detachGifDetail();
    }
  }

  /**
   * Presenter interface implemented by this RIB's view.
   */
  interface TrendingGifPresenter {

    void updateTrendingGifs(List<BrowseGifViewModel> data, int page);

    void showLoadingView();

    void hideLoadingView();

    void showHomeFab();

    void hideHomeFab();

    void setSwipeRefreshListener(SwipeRefreshLayout.OnRefreshListener listener);

    void hideRefreshing();

    void cleanup();

    void scrollToTop();

    Observable<Boolean> loadMore();

    Observable<Object> bringToTopClicks();

    Observable<GifImage> gifClicks();
  }
}
