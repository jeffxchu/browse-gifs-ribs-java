package com.huzu.android.gify.browse;

import com.huzu.android.gify.browse.di.component.AppComponent;
import com.huzu.android.gify.browse.di.component.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import timber.log.Timber;

/**
 * Trending Gif app.
 */
public class App extends DaggerApplication {

  private AppComponent appComponent;

  @Override
  protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
    appComponent = DaggerAppComponent.builder().application(this).build();
    return appComponent;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    if (BuildConfig.DEBUG) {
      Timber.plant(new Timber.DebugTree());
    }
  }

  public AppComponent getAppComponent() {
    return appComponent;
  }
}
