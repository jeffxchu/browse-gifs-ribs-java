package com.huzu.android.gify.browse.di.module;

import com.huzu.android.gify.browse.root.RootActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

  @ContributesAndroidInjector
  abstract RootActivity contributeRootActivity();
}
