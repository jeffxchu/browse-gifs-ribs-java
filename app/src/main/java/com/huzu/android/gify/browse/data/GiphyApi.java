package com.huzu.android.gify.browse.data;

import com.huzu.android.gify.browse.data.model.GifResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * API for handling giphy endpoints.
 */
public interface GiphyApi {

  String API_KEY = "996c4ff2f62a40e1b2b42b7a8de6357b";
  String BASE_URL = "https://api.giphy.com/";

  /**
   * Gets the trending gifs.
   * e.g. https://api.giphy.com/v1/gifs/trending?api_key=0000&limit=25&rating=G
   *
   * @param apiKey an api key.
   * @param limit  the limit.
   * @param offset the offset.
   * @param rating the rating.
   * @return an {@link Observable} for the trending gifs.
   */
  @GET("/v1/gifs/trending")
  Observable<GifResponse> getTrending(
      @Query("api_key") String apiKey,
      @Query("limit") int limit,
      @Query("offset") int offset,
      @Query("rating") String rating);


  /**
   * Searches gifs.
   * e.g. https://api.giphy.com/v1/gifs/search?api_key=0000&q=cat&rating=G
   *
   * @param apiKey an api key.
   * @param query  the query.
   * @param limit the limit.
   * @param rating the rating.
   * @return an {@link Observable} for the gifs.
   */
  @GET("/v1/gifs/search")
  Observable<GifResponse> search(
      @Query("api_key") String apiKey,
      @Query("q") String query,
      @Query("limit") int limit,
      @Query("rating") String rating);
}
