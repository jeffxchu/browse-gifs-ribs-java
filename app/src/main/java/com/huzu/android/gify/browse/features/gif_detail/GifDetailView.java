package com.huzu.android.gify.browse.features.gif_detail;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.huzu.android.gify.browse.R;
import com.huzu.android.gify.browse.data.model.GifImage;
import com.jakewharton.rxbinding2.view.RxView;

import androidx.annotation.Nullable;
import io.reactivex.Observable;

/**
 * Top level view for {@link GifDetailBuilder.GifDetailScope}.
 */
class GifDetailView extends FrameLayout implements GifDetailInteractor.GifDetailPresenter {

  private ImageView imageView;
  private ImageButton imageButton;

  public GifDetailView(Context context) {
    this(context, null);
  }

  public GifDetailView(Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public GifDetailView(Context context, @Nullable AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    imageView = findViewById(R.id.imageViewGifDetail);
    imageButton = findViewById(R.id.buttonDismiss);
  }

  @Override
  public void renderGif(GifImage image) {
    Glide
        .with(imageView.getContext())
        .asGif()
        .load(image.url)
        .placeholder(R.drawable.ic_sentiment_very_satisfied)
        .fitCenter()
        .into(imageView);
  }

  @Override
  public Observable<Object> dismissClicks() {
    return Observable.merge(RxView.clicks(imageButton), RxView.clicks(this));
  }
}
