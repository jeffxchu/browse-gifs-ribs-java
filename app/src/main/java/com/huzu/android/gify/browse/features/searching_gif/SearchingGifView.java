package com.huzu.android.gify.browse.features.searching_gif;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.huzu.android.gify.browse.R;
import com.huzu.android.gify.browse.data.model.GifImage;
import com.huzu.android.gify.browse.ui.BrowseGifAdapter;
import com.huzu.android.gify.browse.ui.BrowseGifViewModel;
import com.huzu.android.gify.browse.ui.GridSpacingItemDecoration;
import com.huzu.android.gify.browse.util.UiUtil;

import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Observable;

import static com.huzu.android.gify.browse.util.Constants.DEFAULT_THROTTLE;

/**
 * Top level view for {@link SearchingGifBuilder.SearchingGifScope}.
 */
public class SearchingGifView extends FrameLayout implements SearchingGifInteractor.SearchingGifPresenter {

  private final BrowseGifAdapter adapter = new BrowseGifAdapter();

  private View loadingView;
  private View emptyView;
  private RecyclerView recyclerView;

  public SearchingGifView(Context context) {
    this(context, null);
  }

  public SearchingGifView(Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public SearchingGifView(Context context, @Nullable AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    recyclerView = findViewById(R.id.recyclerViewSearch);
    recyclerView.setAdapter(adapter);
    int spacing = getResources().getDimensionPixelSize(R.dimen.default_space_half);
    recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, spacing, true));
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (dy > 0) {
          UiUtil.hideKeyboard(getContext(), recyclerView);
        }
      }
    });

    loadingView = findViewById(R.id.loadingView);
    emptyView = findViewById(R.id.textViewEmpty);
  }

  @Override
  public void setData(List<BrowseGifViewModel> data) {
    adapter.setData(data);
  }

  @Override
  public void showLoadingView() {
    loadingView.setVisibility(VISIBLE);
  }

  @Override
  public void hideLoadingView() {
    loadingView.setVisibility(GONE);
  }

  @Override
  public void showEmptyView() {
    emptyView.setVisibility(VISIBLE);
  }

  @Override
  public void hideEmptyView() {
    emptyView.setVisibility(GONE);
  }

  @Override
  public Observable<GifImage> gifClicks() {
    return adapter
        .imageClicks()
        .throttleFirst(DEFAULT_THROTTLE, TimeUnit.MILLISECONDS)
        .doOnNext(__ -> UiUtil.hideKeyboard(getContext(), recyclerView));
  }
}
