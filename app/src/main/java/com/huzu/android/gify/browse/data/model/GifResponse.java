package com.huzu.android.gify.browse.data.model;

import java.util.List;

public final class GifResponse {

  public List<GifData> data;

  public Pagination pagination;

  public MetaData metaData;
}
