package com.huzu.android.gify.browse.root;

import android.view.ViewGroup;

import com.huzu.android.gify.browse.features.search_bar.SearchBarBuilder;
import com.huzu.android.gify.browse.features.search_bar.SearchBarRouter;
import com.huzu.android.gify.browse.features.search_bar.SearchBarView;
import com.huzu.android.gify.browse.features.searching_gif.SearchingGifBuilder;
import com.huzu.android.gify.browse.features.searching_gif.SearchingGifRouter;
import com.huzu.android.gify.browse.features.searching_gif.SearchingGifView;
import com.huzu.android.gify.browse.features.trending_gif.TrendingGifBuilder;
import com.huzu.android.gify.browse.features.trending_gif.TrendingGifRouter;
import com.huzu.android.gify.browse.features.trending_gif.TrendingGifView;
import com.huzu.android.gify.browse.utils.RibTestBase;
import com.uber.rib.core.RouterHelper;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RootRouterTest extends RibTestBase {

  @Mock
  RootView view;
  @Mock
  RootInteractor interactor;
  @Mock
  RootBuilder.Component component;
  @Mock
  TrendingGifBuilder trendingGifBuilder;
  @Mock
  SearchingGifBuilder searchingGifBuilder;
  @Mock
  SearchBarBuilder searchBarBuilder;
  @Mock
  ViewGroup topContainer;
  @Mock
  ViewGroup contentContainer;

  private RootRouter router;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);

    router = new RootRouter(
        view,
        interactor,
        component,
        trendingGifBuilder,
        searchingGifBuilder,
        searchBarBuilder);

    when(view.getTopContainer()).thenReturn(topContainer);
    when(view.getContentContainer()).thenReturn(contentContainer);
  }

  @Test
  public void attachSearchBar_whenInvoked_attachesSearchBar() {
    when(searchBarBuilder.build(any())).thenReturn(mock(SearchBarRouter.class));
    RouterHelper.attach(router);

    router.attachSearchBar();

    verify(searchBarBuilder).build(topContainer);
    verify(topContainer).addView(any(SearchBarView.class));

    RouterHelper.detach(router);
  }

  @Test
  public void detachSearchBar_whenInvoked_detachesSearchBar() {
    when(searchBarBuilder.build(any())).thenReturn(mock(SearchBarRouter.class));
    RouterHelper.attach(router);

    router.attachSearchBar();
    router.detachSearchBar();

    verify(topContainer).removeView(any(SearchBarView.class));

    RouterHelper.detach(router);
  }

  @Test
  public void attachTrendingGif_whenInvoked_attachesTrendingGif() {
    when(trendingGifBuilder.build(any())).thenReturn(mock(TrendingGifRouter.class));
    RouterHelper.attach(router);

    router.attachTrendingGif();

    verify(trendingGifBuilder).build(contentContainer);
    verify(contentContainer).addView(any(TrendingGifView.class));

    RouterHelper.detach(router);
  }

  @Test
  public void detachTrendingGif_whenInvoked_detachesTrendingGif() {
    when(trendingGifBuilder.build(any())).thenReturn(mock(TrendingGifRouter.class));
    RouterHelper.attach(router);

    router.attachTrendingGif();
    router.detachTrendingGif();

    verify(contentContainer).removeView(any(TrendingGifView.class));

    RouterHelper.detach(router);
  }

  @Test
  public void attachSearchingGif_whenInvoked_attachesSearchingGif() {
    when(searchingGifBuilder.build(any())).thenReturn(mock(SearchingGifRouter.class));
    RouterHelper.attach(router);

    router.attachSearchingGif();

    verify(searchingGifBuilder).build(contentContainer);
    verify(contentContainer).addView(any(SearchingGifView.class));

    RouterHelper.detach(router);
  }

  @Test
  public void detachSearchingGif_whenInvoked_detachesSearchingGif() {
    when(searchingGifBuilder.build(any())).thenReturn(mock(SearchingGifRouter.class));
    RouterHelper.attach(router);

    router.attachSearchingGif();
    router.detachSearchingGif();

    verify(contentContainer).removeView(any(SearchingGifView.class));

    RouterHelper.detach(router);
  }
}
