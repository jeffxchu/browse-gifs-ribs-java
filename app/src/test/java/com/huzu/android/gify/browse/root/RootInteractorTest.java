package com.huzu.android.gify.browse.root;

import com.huzu.android.gify.browse.util.SearchQueryStream;
import com.huzu.android.gify.browse.utils.RibTestBase;
import com.uber.rib.core.InteractorHelper;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.subjects.PublishSubject;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RootInteractorTest extends RibTestBase {

  @Mock
  RootInteractor.RootPresenter presenter;
  @Mock
  RootRouter router;
  @Mock
  SearchQueryStream searchQueryStream;

  private final PublishSubject<String> searchSubject = PublishSubject.create();

  private RootInteractor interactor;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);

    interactor = TestRootInteractor.create(presenter, searchQueryStream);

    when(searchQueryStream.queryObservable()).thenReturn(searchSubject);
  }

  @Test
  public void didBecomeActive_whenInteractorAttached_attachesSearchBar() {
    InteractorHelper.attach(interactor, presenter, router, null);

    assertThat(interactor.isAttached()).isTrue();

    verify(router).attachSearchBar();
  }

  @Test
  public void didBecomeActive_whenEmittedQueryStringEmpty_attachesTrendingRib() {
    InteractorHelper.attach(interactor, presenter, router, null);

    searchSubject.onNext("");

    verify(router).attachTrendingGif();
    verify(router).detachSearchingGif();
  }

  @Test
  public void didBecomeActive_whenEmittedQueryStringNoneEmpty_attachesSearchingRib() {
    InteractorHelper.attach(interactor, presenter, router, null);

    searchSubject.onNext("abc");

    verify(router).attachSearchingGif();
    verify(router).detachTrendingGif();
  }

  @Test
  public void willResignActive_whenInvoked_detachesSearchBar() {
    InteractorHelper.attach(interactor, presenter, router, null);
    interactor.willResignActive();
    verify(router).detachSearchBar();
  }

  @Test
  public void willResignActive_whenInvoked_detachesSearchingGif() {
    InteractorHelper.attach(interactor, presenter, router, null);
    interactor.willResignActive();
    verify(router).detachSearchingGif();
  }

  @Test
  public void willResignActive_whenInvoked_detachesTrendingGif() {
    InteractorHelper.attach(interactor, presenter, router, null);
    interactor.willResignActive();
    verify(router).detachTrendingGif();
  }
}